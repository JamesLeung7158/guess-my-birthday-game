from random import randint
#ask for name
name = input("Hi! What is your name?")
print("Hi,", name)
#maximum number of guess attempts
tries = 5
months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

for guess in range(tries):
    #randint for guessing month/year
    month_guess = randint(0,11); year_guess = randint(1924,2004);
    #statement of guess
    print("Guess", guess + 1, ":", name, "were you born in", months[month_guess], "/", year_guess, "?")
    #prompting for correct guess or not
    prompt = input("Was my guess correct? yes or no?")
    if prompt == "yes":
        print("I knew it!")
        break         
    else:
        print("Drat! Lemme try again!")
    if guess == tries - 1:
        print("I have other things to do. Good bye.")

#stretch goals
# prompt = input("Was my guess correct? yes, no, or close?")
# elif prompt == "close":
        # prompt2 = input("Was I high or low?");
        # if prompt2 == "high":
